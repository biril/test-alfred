module.exports = function (grunt) {
	
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		uglify: {
			options: {
				banner: "// Uglified!\n"
			},
			build: {
				files: {
					"../built/built.js": ["source.js"]
				} 
			}
		}
	});

	grunt.loadNpmTasks("grunt-contrib-uglify");

	grunt.registerTask("default", ["uglify:build"]);
	grunt.registerTask("build", ["uglify:build"]);
}
